package com.example.samples.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TreeController {
    @GetMapping("/tree-nodes")
    public ResponseEntity<?>treeNodes(){
        return ResponseEntity.ok().build();
    }
}
