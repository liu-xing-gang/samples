package com.example.samples.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SamplesController {

    /**
     * 文档列表
     *
     * @return
     */
    @GetMapping("/documents")
    public ResponseEntity<?> listDocuments() {
        return ResponseEntity.ok().build();
    }

    /**
     * 创建文档
     *
     * @return
     */
    @PostMapping("/documents")
    public ResponseEntity<?> createDocuments() {
        return ResponseEntity.ok().build();
    }

    /**
     * 文档详情
     *
     * @param documentId
     * @return
     */
    @GetMapping("/documents/{documentId}")
    public ResponseEntity<?> getDocument(@PathVariable String documentId) {
        return ResponseEntity.ok().build();
    }

    /**
     * 修改文档
     *
     * @param documentId
     * @return
     */
    @PutMapping("/documents/{documentId}")
    public ResponseEntity<?> modifyDocument(@PathVariable String documentId) {
        return ResponseEntity.ok().build();
    }

    /**
     * 检入文档
     *
     * @param docuemntId
     * @return
     */
    @PutMapping("/documents/{documentId}/checkin")
    public ResponseEntity<?> checkinDocument(@PathVariable String docuemntId) {
        return ResponseEntity.ok().build();
    }

    /**
     * 删除文档
     *
     * @param documentId
     * @return
     */
    @DeleteMapping("/documents/{documentId}")
    public ResponseEntity<?> deleteDocument(@PathVariable String documentId) {
        return ResponseEntity.ok().build();
    }
}
